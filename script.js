class Environment{
    constructor(ctx,obstacles, grid_size, rows, columns){
        this.ctx = ctx;
        this.grid_size = grid_size;
        this.rows = rows;
        this.columns = columns;
        this.grid = [];
        this.gridArray = [];
        this.obstacles = obstacles;
    }

    DrawCube(X,Y){
        this.ctx.beginPath();
        this.ctx.rect(X, Y, this.grid_size, this.grid_size);
        this.ctx.stroke();
    }
    DrawGrid(){
        for (let r = 0; r < this.columns; r++) {
            if(!this.grid[r]){
                this.grid.push([]);
            }
            for (let c = 0; c < this.rows; c++) {
                this.grid[r].push({x:r*this.grid_size,y:c*this.grid_size})
                this.DrawCube(r*this.grid_size,c*this.grid_size);
            }
        }
        this.Obstacles();
    }
    PaintCubeByCoordenate(rol,col,color){
        this.ctx.beginPath();
        this.ctx.fillStyle = color;
        this.ctx.fillRect(this.grid[rol][col].x, this.grid[rol][col].y, this.grid_size, this.grid_size);
        this.ctx.stroke();
    }

    Obstacles(){
        this.obstacles.forEach(element => {
            this.PaintCubeByCoordenate(element[0],element[1],element[2]);
        });
    }

    isObstacle(x,y){
        let value = this.obstacles.find(element => {
            if(element[0] == x && element[1] == y){
                return true;
            }
            return false;
        });
        return value;
    }
}

var obstacles_1 = [
    [0,0,"#3185FC","start"],
    [3,0,"#000000","obstacle"],
    [3,2,"#000000","obstacle"],
    [3,4,"#000000","obstacle"],
    [3,6,"#000000","obstacle"],
    [3,7,"#000000","obstacle"],
    [5,1,"#000000","obstacle"],
    [5,3,"#000000","obstacle"],
    [5,4,"#000000","obstacle"],
    [5,6,"#000000","obstacle"],
    [5,7,"#000000","obstacle"],
    [7,7,"red","end"]
];
var obstacles_2 = [
    [0,0,"#3185FC","start"],
    [3,0,"#000000","obstacle"],
    [3,2,"#000000","obstacle"],
    [3,4,"#000000","obstacle"],
    [3,5,"#000000","obstacle"],
    [3,6,"#000000","obstacle"],
    [3,7,"#000000","obstacle"],
    [3,8,"#000000","obstacle"],
    [5,0,"#000000","obstacle"],
    [5,1,"#000000","obstacle"],
    [5,3,"#000000","obstacle"],
    [5,6,"#000000","obstacle"],
    [5,7,"#000000","obstacle"],
    [7,0,"#000000","obstacle"],
    [7,1,"#000000","obstacle"],
    [7,2,"#000000","obstacle"],
    [7,3,"#000000","obstacle"],
    [7,5,"#000000","obstacle"],
    [7,6,"#000000","obstacle"],
    [7,7,"#000000","obstacle"],
    [7,8,"#000000","obstacle"],
    [9,9,"red","end"]
];